package com.example.snapchat

import android.Manifest
import android.app.Activity
import android.content.Intent
import android.content.pm.PackageManager
import android.graphics.Bitmap
import android.graphics.drawable.BitmapDrawable
import android.net.Uri
import android.os.Build
import android.os.Bundle
import android.provider.MediaStore
import android.util.Log
import android.widget.Toast
import androidx.annotation.RequiresApi
import androidx.appcompat.app.AppCompatActivity
import com.example.snapchat.databinding.ActivityCreateSnapBinding
import com.google.android.gms.tasks.OnFailureListener
import com.google.android.gms.tasks.OnSuccessListener
import com.google.android.gms.tasks.Task
import com.google.firebase.storage.FirebaseStorage
import com.google.firebase.storage.UploadTask
import java.io.ByteArrayOutputStream
import java.util.*


class CreateSnapActivity : AppCompatActivity() {

    private lateinit var binding: ActivityCreateSnapBinding
    private val imageName = UUID.randomUUID().toString() + ".jpg"

    @RequiresApi(Build.VERSION_CODES.M)
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        binding = ActivityCreateSnapBinding.inflate(layoutInflater)
        val view = binding.root
        setContentView(view)

        binding.chooseImageButton.setOnClickListener {
            if (checkSelfPermission(Manifest.permission.READ_EXTERNAL_STORAGE) != PackageManager.PERMISSION_GRANTED) {
                requestPermissions(arrayOf(Manifest.permission.READ_EXTERNAL_STORAGE), 1)
            } else {
                getPhoto()
            }
        }

        binding.uploadSnap.setOnClickListener {
            // Get the data from an ImageView as bytes
            binding.imageView.isDrawingCacheEnabled = true
            binding.imageView.buildDrawingCache()
            val bitmap = (binding.imageView.drawable as BitmapDrawable).bitmap
            val baos = ByteArrayOutputStream()
            bitmap.compress(Bitmap.CompressFormat.JPEG, 100, baos)
            val data = baos.toByteArray()

            val uploadTask: UploadTask =
                FirebaseStorage.getInstance().reference.child("images").child(imageName)
                    .putBytes(data)

            var imageURL = "abc"
            uploadTask
                .addOnFailureListener(OnFailureListener {
                    Toast.makeText(this, "Upload Failure", Toast.LENGTH_SHORT).show()
                })
                .addOnSuccessListener(OnSuccessListener<UploadTask.TaskSnapshot>() { taskSnapshot ->
                    Toast.makeText(this, "Upload Success", Toast.LENGTH_SHORT).show()
                    val intent = Intent(this, UsersActivity::class.java)
                    if (taskSnapshot.metadata != null) {
                        if (taskSnapshot.metadata!!.reference != null) {
                            val result: Task<Uri> = taskSnapshot.storage.downloadUrl;
                            result.addOnSuccessListener(OnSuccessListener<Uri>() { url ->
                                val imageUrl: String = url.toString()
                                Log.d("Url", imageUrl)
                                imageURL = imageUrl
                                intent.putExtra("imageURL", imageURL)
                                intent.putExtra("imageName", imageName)
                                intent.putExtra("message", binding.snapMessageEditText.text.toString())
                                startActivity(intent)
                            })
                        }
                    }



                })
        }
    }

    private fun getPhoto() {
        val intent = Intent(Intent.ACTION_PICK, MediaStore.Images.Media.EXTERNAL_CONTENT_URI)
        startActivityForResult(intent, 1)
    }

    override fun onRequestPermissionsResult(
        requestCode: Int,
        permissions: Array<out String>,
        grantResults: IntArray
    ) {
        super.onRequestPermissionsResult(requestCode, permissions, grantResults)
        if (requestCode == 1) {
            if (grantResults.isNotEmpty() && grantResults[0] == PackageManager.PERMISSION_GRANTED) {
                getPhoto()
            }
        }
    }

    override fun onActivityResult(requestCode: Int, resultCode: Int, data: Intent?) {
        super.onActivityResult(requestCode, resultCode, data)

        val selectedImage = data?.data

        if (requestCode == 1 && resultCode == Activity.RESULT_OK && data != null) {
            try {
                val bitmap =
                    MediaStore.Images.Media.getBitmap(this.contentResolver, selectedImage)
                binding.imageView.setImageBitmap(bitmap)
            } catch (e: Exception) {
                e.printStackTrace()
            }
        }
    }
}