package com.example.snapchat

import android.content.Intent
import android.view.LayoutInflater
import android.view.ViewGroup
import androidx.core.content.ContextCompat
import androidx.recyclerview.widget.RecyclerView
import com.example.snapchat.databinding.AdapterUsersBinding
import com.google.firebase.auth.FirebaseAuth
import com.google.firebase.database.FirebaseDatabase

class RecyclerViewAdapter(
    private var emails: MutableList<String>,
    private var ids: MutableList<String>,
    private val intent: Intent
) :
    RecyclerView.Adapter<RecyclerViewAdapter.ViewHolder>() {


    override fun onCreateViewHolder(
        parent: ViewGroup,
        viewType: Int
    ): RecyclerViewAdapter.ViewHolder {
        val itemBinding =
            AdapterUsersBinding.inflate(LayoutInflater.from(parent.context), parent, false)
        return ViewHolder(itemBinding)
    }


    override fun onBindViewHolder(holder: RecyclerViewAdapter.ViewHolder, position: Int) {
        return holder.onBind(emails[position])
    }

    override fun getItemCount(): Int = emails.size


    fun setAdapterData(adapterItems: MutableList<String>, keys: MutableList<String>) {
        this.emails = adapterItems
        this.ids = keys
        this.notifyDataSetChanged()
    }

    inner class ViewHolder(private val itemBinding: AdapterUsersBinding) :
        RecyclerView.ViewHolder(itemBinding.root) {
        fun onBind(email: String) = with(itemBinding) {
            userEmail.text = email
            root.setOnClickListener {
                val snapMap: Map<String, String> =
                    mapOf(
                        "from" to FirebaseAuth.getInstance().currentUser?.email.toString(),
                        "imageName" to intent.getStringExtra("imageName") as String,
                        "imageURL" to intent.getStringExtra("imageURL") as String,
                        "message" to intent.getStringExtra("message") as String
                    )

                FirebaseDatabase.getInstance().reference.child("users").child(ids[adapterPosition])
                    .child("snaps").push().setValue(snapMap)

                val intent = Intent(itemView.context, SnapchatActivity::class.java)
                intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP)
                ContextCompat.startActivity(itemView.context, intent, null)
            }
        }
    }
}