package com.example.snapchat

import android.content.Intent
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.view.LayoutInflater
import android.view.Menu
import android.view.MenuItem
import android.view.ViewGroup
import androidx.core.content.ContextCompat
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.recyclerview.widget.RecyclerView
import com.example.snapchat.databinding.ActivitySnapchatBinding
import com.example.snapchat.databinding.AdapterSnapsBinding
import com.google.firebase.auth.FirebaseAuth
import com.google.firebase.auth.ktx.auth
import com.google.firebase.database.ChildEventListener
import com.google.firebase.database.DataSnapshot
import com.google.firebase.database.DatabaseError
import com.google.firebase.database.FirebaseDatabase
import com.google.firebase.ktx.Firebase

class SnapchatActivity : AppCompatActivity() {

    private lateinit var binding: ActivitySnapchatBinding

    private lateinit var auth: FirebaseAuth

    private lateinit var adapter: SnapsRecyclerViewAdapter

    private var emails = mutableListOf<String>()
    private var snaps = mutableListOf<DataSnapshot>()

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
//        window.setBackgroundDrawable(ResourcesCompat.getDrawable(resources, R.drawable.window_background_yellow, null))
        binding = ActivitySnapchatBinding.inflate(layoutInflater)
        val view = binding.root
        setContentView(view)

        auth = Firebase.auth

        binding.addSnaps.setOnClickListener {
            startActivity(Intent(this, CreateSnapActivity::class.java))
        }

        setAdapter()
        getSnaps()
    }

    private fun setAdapter() {
        adapter = SnapsRecyclerViewAdapter(mutableListOf(), mutableListOf())
        binding.snapsRecyclerView.layoutManager = LinearLayoutManager(this)
        binding.snapsRecyclerView.adapter = adapter
    }

    private fun getSnaps() {
        FirebaseDatabase.getInstance().reference.child("users").child(auth.currentUser!!.uid).child("snaps").addChildEventListener(
            object : ChildEventListener {
                override fun onChildAdded(snapshot: DataSnapshot, previousChildName: String?) {
                    emails.add(snapshot.child("from").value as String)
                    snaps.add(snapshot)
                    adapter.setAdapterData(emails, snaps)
                }
                override fun onChildChanged(snapshot: DataSnapshot, previousChildName: String?) {}
                override fun onChildRemoved(snapshot: DataSnapshot) {
                    var index = 0
                    for (snap: DataSnapshot in snaps) {
                        if (snap.key == snapshot.key) {
                            snaps.removeAt(index)
                            emails.removeAt(index)
                            break
                        }
                        index++
                    }
                    adapter.notifyDataSetChanged()
                }
                override fun onChildMoved(snapshot: DataSnapshot, previousChildName: String?) {}
                override fun onCancelled(error: DatabaseError) {}
            })
    }

    override fun onCreateOptionsMenu(menu: Menu?): Boolean {
        val inflater = menuInflater
        inflater.inflate(R.menu.menu, menu)
        return super.onCreateOptionsMenu(menu)
    }

    override fun onOptionsItemSelected(item: MenuItem): Boolean {
        if (item.itemId == R.id.logOut) {
            auth.signOut()
            startActivity(Intent(this, SplashScreenActivity::class.java))
            finish()
        }
        return super.onOptionsItemSelected(item)
    }

    class SnapsRecyclerViewAdapter(
        private var emails: MutableList<String>,
        private var snaps: MutableList<DataSnapshot>
    ) :
        RecyclerView.Adapter<SnapsRecyclerViewAdapter.ViewHolder>() {


        override fun onCreateViewHolder(
            parent: ViewGroup,
            viewType: Int
        ): SnapsRecyclerViewAdapter.ViewHolder {
            val itemBinding =
                AdapterSnapsBinding.inflate(LayoutInflater.from(parent.context), parent, false)
            return ViewHolder(itemBinding)
        }


        override fun onBindViewHolder(holder: SnapsRecyclerViewAdapter.ViewHolder, position: Int) {
            return holder.onBind(emails[position], snaps[position])
        }

        override fun getItemCount(): Int = emails.size


        fun setAdapterData(adapterItems: MutableList<String>, snaps: MutableList<DataSnapshot>) {
            this.emails = adapterItems
            this.snaps = snaps
            this.notifyDataSetChanged()
        }

        inner class ViewHolder(private val itemBinding: AdapterSnapsBinding) :
            RecyclerView.ViewHolder(itemBinding.root) {
            fun onBind(email: String, snap: DataSnapshot) = with(itemBinding) {
                snaps.text = email
                root.setOnClickListener {
                    val intent = Intent(itemView.context, ViewSnapsActivity::class.java)
                    intent.putExtra("imageName", snap.child("imageName").value as String)
                    intent.putExtra("imageURL", snap.child("imageURL").value as String)
                    intent.putExtra("message", snap.child("message").value as String)
                    intent.putExtra("snapKey", snap.key)
                    ContextCompat.startActivity(itemView.context, intent, null)
                }
            }
        }
    }
}