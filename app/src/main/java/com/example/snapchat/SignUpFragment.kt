package com.example.snapchat

import android.os.Bundle
import androidx.fragment.app.Fragment
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.Toast
import com.example.snapchat.databinding.FragmentSignUpBinding
import com.example.snapchat.databinding.FragmentWelcomeBinding
import com.google.firebase.auth.FirebaseAuth
import com.google.firebase.auth.ktx.auth
import com.google.firebase.database.FirebaseDatabase
import com.google.firebase.ktx.Firebase

class SignUpFragment : Fragment() {

    private var _binding: FragmentSignUpBinding? = null

    // This property is only valid between onCreateView and
    // onDestroyView.
    private val binding get() = _binding!!

    private lateinit var auth: FirebaseAuth

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View {
        _binding = FragmentSignUpBinding.inflate(inflater, container, false)
        return binding.root
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        auth = Firebase.auth
        binding.closeDialogIV.setOnClickListener {
            requireActivity().onBackPressed()
        }
        binding.signUpUserButton.setOnClickListener {
            auth.createUserWithEmailAndPassword(
                binding.signUpEmailEditText.text.toString(),
                binding.signUpPasswordEditText.text.toString()
            )
                .addOnCompleteListener(requireActivity()) { task ->
                    if (task.isSuccessful) {
                        // Sign in success, update UI with the signed-in user's information
                        val user = auth.currentUser
                        FirebaseDatabase.getInstance().reference.child("users").child(task.result!!.user?.uid!!).child("email").setValue(binding.signUpEmailEditText.text.toString())
                        (activity as SplashScreenActivity).updateUI(user)
                    } else {
                        // If sign in fails, display a message to the user.
                        Toast.makeText(requireContext(), "Sign Up failed.", Toast.LENGTH_SHORT).show()
                        (activity as SplashScreenActivity).updateUI(null)
                    }
                }
        }
    }

    override fun onDestroyView() {
        super.onDestroyView()
        _binding = null
    }
}