package com.example.snapchat

import android.os.Bundle
import androidx.fragment.app.Fragment
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.Toast
import com.example.snapchat.databinding.FragmentLogInBinding
import com.example.snapchat.databinding.FragmentWelcomeBinding
import com.google.firebase.auth.FirebaseAuth
import com.google.firebase.auth.ktx.auth
import com.google.firebase.ktx.Firebase

class LogInFragment : Fragment() {

    private var _binding: FragmentLogInBinding? = null

    // This property is only valid between onCreateView and
    // onDestroyView.
    private val binding get() = _binding!!

    private lateinit var auth: FirebaseAuth

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View {
        _binding = FragmentLogInBinding.inflate(inflater, container, false)
        return binding.root
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        auth = Firebase.auth
        binding.closeDialogIV.setOnClickListener {
            requireActivity().onBackPressed()
        }
        binding.logInUserButton.setOnClickListener {
            auth.signInWithEmailAndPassword(
                binding.logInEmailEditText.text.toString(),
                binding.logInPasswordEditText.text.toString()
            )
                .addOnCompleteListener(requireActivity()) { task ->
                    if (task.isSuccessful) {
                        // Sign in success, update UI with the signed-in user's information
                        val user = auth.currentUser
                        (activity as SplashScreenActivity).updateUI(user)
                    } else {
                        // If sign in fails, display a message to the user.
                        Toast.makeText(requireContext(), "Log In failed.", Toast.LENGTH_SHORT).show()
                        (activity as SplashScreenActivity).updateUI(null)
                    }
                }
        }
    }

    override fun onDestroyView() {
        super.onDestroyView()
        _binding = null
    }
}